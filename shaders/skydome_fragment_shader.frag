#version 330

uniform sampler2D texture_image;
in vec2 view_textures;
out vec4 frag_color;

void main ()
{
	frag_color = texture(texture_image, view_textures);
}
