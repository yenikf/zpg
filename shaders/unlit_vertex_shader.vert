#version 330

uniform mat4 model_position_matrix;
uniform mat4 camera_view_matrix;
uniform mat4 projection_matrix;
layout (location = 0) in vec3 input_vertex;
layout (location = 1) in vec3 input_normal;
layout (location = 2) in vec2 texture_coordinates;
out vec2 view_textures;

mat3 normal_inverse_matrix(mat4 model_position_matrix)
{
	return transpose(inverse(mat3(model_position_matrix)));
}

void main ()
{
	gl_Position = projection_matrix * camera_view_matrix * model_position_matrix * vec4(input_vertex, 1.0);
	view_textures = texture_coordinates;
}
