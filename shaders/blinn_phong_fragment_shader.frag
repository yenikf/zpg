#version 330

uniform sampler2D texture_image;
in vec4 view_vertex;
in vec4 view_normal;
in vec2 view_textures;
in vec4 view_light_position;
out vec4 frag_color;

float ambient_light()
{
	return 0.1;
}

float diffuse_light(vec4 light_direction, vec4 surface_normal)
{
	return max(dot(light_direction, normalize(surface_normal)), 0.0);
}

float specular_light(vec4 light_direction, vec4 surface_normal, vec4 eye_direction)
{
	float sharpness = 32;
	vec4 half_vector = normalize(light_direction + eye_direction);
	float reflection = max(dot(surface_normal, half_vector), 0.0);
	return pow(reflection, sharpness);
}

float light_intensity(float light_irradiance, float light_source_distance)
{
	float coefficient = 0.001;
	return light_irradiance / (1 + coefficient * length(light_source_distance));
}

void main ()
{
	vec4 surface_color = vec4(0.385, 0.647, 0.812, 1.0);

	vec4 light_source_vector = normalize(view_light_position - view_vertex);
	float light_source_distance = length(view_light_position - view_vertex);
	vec4 eye_position_vector = normalize(vec4(0.0, 0.0, 0.0, 1.0) - view_vertex); // view position is always at the origin

//	frag_color = (specular_light(light_source_vector, view_normal, eye_position_vector) +
//	              + diffuse_light(light_source_vector, view_normal)
//	              + ambient_light())
//	        * surface_color
//	        * light_intensity(4, light_source_distance);
	float light_strength = (specular_light(light_source_vector, view_normal, eye_position_vector)
	                        + diffuse_light(light_source_vector, view_normal)
	                        + ambient_light())
	        * light_intensity(1, light_source_distance);
//	frag_color = texture(texture_image, view_textures) * light_strength * surface_color;
	frag_color = texture(texture_image, view_textures) * light_strength;
}
