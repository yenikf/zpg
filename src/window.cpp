#include "window.h"

#include <glm/vec3.hpp>

#include <iostream>

#include "entity.h"
#include "renderer.h"


Window::Window(const Renderer& renderer, int windowWidth, int windowHeight, const char* windowTitle)
	: mRenderer(renderer)
	, mWidth(windowWidth)
	, mHeight(windowHeight)
	, mCamera(renderer, windowWidth, windowHeight)
{
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	mWindowHandle = glfwCreateWindow(windowWidth, windowHeight, windowTitle, nullptr, nullptr);
	if (!mWindowHandle) {
		std::cerr << "ERROR: could not create window\n";
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(mWindowHandle);
	glfwSwapInterval(1);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
}


Window::~Window()
{
	glfwDestroyWindow(mWindowHandle);
}


void Window::changeWindowSize(int width, int height)
{
	mWidth = width;
	mHeight = height;
	glViewport(0, 0, width, height);
	mCamera.setProjection(width, height);
}


void Window::close()
{
	glfwSetWindowShouldClose(mWindowHandle, GL_TRUE);
}


bool Window::shouldClose() const
{
	return glfwWindowShouldClose(mWindowHandle);
}


void Window::clearScreen()
{
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


void Window::finishFrame()
{
	glfwSwapBuffers(mWindowHandle);
}


// NOTE Perhaps add attachDefaultCamera or similar, too.
void Window::registerCameraObject(Entity* newCamera)
{
	mCameraEntities.push_back(newCamera);
}


void Window::switchCamera()
{
	if (!mCameraEntities.empty()) {
		mActiveCameraIndex = (mActiveCameraIndex + 1) % mCameraEntities.size();
		mCamera.setPosition(mCameraEntities[mActiveCameraIndex]);
	}
}


void Window::startCamera()
{
	if (!mCameraEntities.empty()) {
		mCamera.setPosition(mCameraEntities[mActiveCameraIndex]);
		mCamera.setProjection(mWidth, mHeight);
	} else {
		std::cerr << "Can't start camera - no cameras registered.\n";
	}
}


Camera& Window::getActiveCamera()
{
	return mCamera;
}


std::pair<int, glm::vec3> Window::identifyObject(bool isAtCenter)
{
	double xPos = mWidth / 2;
	double yPos = mHeight / 2;
	if (!isAtCenter) {
		glfwGetCursorPos(mWindowHandle, &xPos, &yPos);
	}
	yPos = mHeight - yPos; // the coordinate is upside down
	auto [index, depth] = mRenderer.identifyObject(xPos, yPos);
	std::cerr << "Clicked on " << xPos << ", " << yPos << ", depth: " << depth << ", index: " << index << '\n';
	glm::vec3 worldCoordinates = mCamera.identifyCoordinates(mWidth, mHeight, xPos, yPos, depth);
	std::cerr << "World coordinates: " << worldCoordinates.x << ", " << worldCoordinates.y << ", " << worldCoordinates.z << '\n';
	return { index, worldCoordinates };
}


void Window::setMouselookView(bool mouseLookActive)
{
	if (mouseLookActive) {
		glfwSetInputMode(mWindowHandle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		glfwSetInputMode(mWindowHandle, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
	} else {
		glfwSetInputMode(mWindowHandle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
}


void Window::setCursorPositionCallback(void cursorCallback(GLFWwindow*, double, double))
{
	glfwSetCursorPosCallback(mWindowHandle, cursorCallback);
}


void Window::setMouseButtonCallback(void buttonCallback(GLFWwindow*, int, int, int))
{
	glfwSetMouseButtonCallback(mWindowHandle, buttonCallback);
}


void Window::setKeypushCallback(void keyCallback(GLFWwindow*, int, int, int, int))
{
	glfwSetKeyCallback(mWindowHandle, keyCallback);
}


void Window::setWindowFocusCallback(void windowFocusCallback(GLFWwindow*, int))
{
	glfwSetWindowFocusCallback(mWindowHandle, windowFocusCallback);
}


void Window::setWindowIconifyCallback(void windowIconifyCallback(GLFWwindow*, int))
{
	glfwSetWindowIconifyCallback(mWindowHandle, windowIconifyCallback);
}


void Window::setWindowSizeCallback(void windowSizeCallback(GLFWwindow*, int, int))
{
	glfwSetWindowSizeCallback(mWindowHandle, windowSizeCallback);
}

