#pragma once

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "mesh.h"


class MeshLibrary
{
public:
	const Mesh* getMesh(const std::string& name);

private:
	std::vector<float> loadMesh(const std::string& name);
	void registerMesh(const std::string& name, const std::vector<float>& vertices);

	static const std::string defaultPath;
	static const std::string defaultType;

	std::unordered_map<std::string, std::unique_ptr<const Mesh>> mMeshInfos;
};

