#include "controller.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/vec3.hpp>

#include <iostream>

#include "scene.h"
#include "window.h"


static void cursorCallback(GLFWwindow* window, double x, double y);
static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mode);
static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
static void windowFocusCallback(GLFWwindow* window, int focused);
static void windowIconifyCallback(GLFWwindow* window, int iconified);
static void windowSizeCallback(GLFWwindow* window, int width, int height);


Controller& Controller::instance()
{
	static Controller sController;
	return sController;
}


void Controller::attachWindow(Window* window)
{
	releaseCallbacks(mControlledWindow);
	mControlledWindow = window;
	setCallbacks(mControlledWindow);
}


void Controller::attachScene(Scene* scene)
{
	mControlledScene = scene;
}


void Controller::handleInput()
{
	Window* window = Controller::instance().mControlledWindow;
	if (!window) {
		return;
	}

	if (wPressed) {
		window->getActiveCamera().moveForward(movementSpeed);
	}
	if (sPressed) {
		window->getActiveCamera().moveForward(-movementSpeed);
	}
	if (aPressed) {
		window->getActiveCamera().moveLeft(movementSpeed);
	}
	if (dPressed) {
		window->getActiveCamera().moveLeft(-movementSpeed);
	}
	if (spacePressed) {
		window->getActiveCamera().moveUp(movementSpeed);
	}
	if (shiftPressed) {
		window->getActiveCamera().moveUp(-movementSpeed);
	}
}


void Controller::onCursorPosition(double x, double y)
{
	//std::cerr << "cursor_callback: x = " << x << ", y = " << y << '\n';

	if (mouseLook) {
		static double lastX;
		static double lastY;
		if (mouseModeSwitched) {
			lastX = x;
			lastY = y;
			mouseModeSwitched = false;
		}

		Window* window = Controller::instance().mControlledWindow;
		if (!window) {
			return;
		}
		window->getActiveCamera().lookUp((lastY - y) / mouseLookDivisor);
		window->getActiveCamera().lookLeft((lastX - x) / mouseLookDivisor);

		lastX = x;
		lastY = y;
	}
}


void Controller::onMouseButton(int button, int action, int mode)
{
	//	std::cerr << "button_callback [" << button << ',' << action << ',' << mode << "]\n";

	if (action == GLFW_PRESS) {
		switch (button) {
			case 0:
				break;
			case 1:
				break;
		}
	}
	if (action == GLFW_RELEASE) {
		switch (button) {
			case 0:
				{
					Window* window = Controller::instance().mControlledWindow;
					if (window) {
						auto [ index, coordinates ] = window->identifyObject(mouseLook);
						Scene* scene = Controller::instance().mControlledScene;
						if (scene) {
							scene->actionAt(index, coordinates);
						}
					}
				}
				break;
			case 1:
				mouseLook ^= true;
				{
					Window* window = Controller::instance().mControlledWindow;
					if (window) {
						window->setMouselookView(mouseLook);
					}
				}
				break;
		}
		mouseModeSwitched = true;
	}
}


void Controller::onKeyPress(int key, int scancode, int action, int mods)
{
	//	std::cerr << "key_callback [" << key << ',' << scancode << ',' << action << ',' << mods << "]\n";

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		Window* window = Controller::instance().mControlledWindow;
		if (window) {
			window->close();
		}
	} else if (key == GLFW_KEY_C && action == GLFW_PRESS) {
		// TODO Tidy up the switchCamera control.
		Window* window = Controller::instance().mControlledWindow;
		if (window) {
			window->switchCamera();
		}
	} else if (key == GLFW_KEY_UP && action == GLFW_PRESS) {
		wPressed = true;
	} else if (key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
		sPressed = true;
	} else if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
		aPressed = true;
	} else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
		dPressed = true;
	} else if (key == GLFW_KEY_PAGE_UP && action == GLFW_PRESS) {
		spacePressed = true;
	} else if (key == GLFW_KEY_PAGE_DOWN && action == GLFW_PRESS) {
		shiftPressed = true;
	} else if (key == GLFW_KEY_UP && action == GLFW_RELEASE) {
		wPressed = false;
	} else if (key == GLFW_KEY_DOWN && action == GLFW_RELEASE) {
		sPressed = false;
	} else if (key == GLFW_KEY_LEFT && action == GLFW_RELEASE) {
		aPressed = false;
	} else if (key == GLFW_KEY_RIGHT && action == GLFW_RELEASE) {
		dPressed = false;
	} else if (key == GLFW_KEY_PAGE_UP && action == GLFW_RELEASE) {
		spacePressed = false;
	} else if (key == GLFW_KEY_PAGE_DOWN && action == GLFW_RELEASE) {
		shiftPressed = false;
	}
}


void Controller::onWindowFocus(int focused)
{
	//	std::cerr << "window_focus_callback\n";
		// TODO if we ever allow for more open windows, handle the focus by switching the active controlled window
}


void Controller::onWindowIconify(int iconified)
{
	//	std::cerr << "window_iconify_callback\n";
}


void Controller::onWindowSizeChange(int width, int height)
{
	//	std::cerr << "resize " << width << ", " << height << '\n';

	Window* window = Controller::instance().mControlledWindow;
	if (window) {
		window->changeWindowSize(width, height);
	}
}


void Controller::setCallbacks(Window* window)
{
	if (window) {
		window->setCursorPositionCallback(cursorCallback);
		window->setMouseButtonCallback(mouseButtonCallback);
		window->setKeypushCallback(keyCallback);
		window->setWindowFocusCallback(windowFocusCallback);
		window->setWindowIconifyCallback(windowIconifyCallback);
		window->setWindowSizeCallback(windowSizeCallback);
	}
}


void Controller::releaseCallbacks(Window* window)
{
	if (window) {
		window->setCursorPositionCallback(nullptr);
		window->setMouseButtonCallback(nullptr);
		window->setKeypushCallback(nullptr);
		window->setWindowFocusCallback(nullptr);
		window->setWindowIconifyCallback(nullptr);
		window->setWindowSizeCallback(nullptr);
	}
}


bool Controller::wPressed{};
bool Controller::sPressed{};
bool Controller::aPressed{};
bool Controller::dPressed{};
bool Controller::spacePressed{};
bool Controller::shiftPressed{};
bool Controller::mouseLook{};
bool Controller::mouseModeSwitched{};


static void cursorCallback(GLFWwindow* window, double x, double y)
{
	Controller::instance().onCursorPosition(x, y);
}


static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mode)
{
	Controller::instance().onMouseButton(button, action, mode);
}


static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	Controller::instance().onKeyPress(key, scancode, action, mods);
}


static void windowFocusCallback(GLFWwindow* window, int focused)
{
	Controller::instance().onWindowFocus(focused);
}


static void windowIconifyCallback(GLFWwindow* window, int iconified)
{
	Controller::instance().onWindowIconify(iconified);
}


static void windowSizeCallback(GLFWwindow* window, int width, int height)
{
	Controller::instance().onWindowSizeChange(width, height);
}
