#pragma once

#include "position.h"


class Movable
{
public:
	Movable(Position pos);

	const Position& getPosition() const;
	glm::vec3 getPositionCoordinates() const;

	Movable& moveX(float distance);
	Movable& moveY(float distance);
	Movable& moveZ(float distance);
	Movable& moveForward(float distance);
	Movable& moveLeft(float distance);
	Movable& moveUp(float distance);
	Movable& lookUp(float degrees);
	Movable& lookLeft(float degrees);
	Movable& scale(float factor);

private:
	Position mPosition;
};
