#include "model_render_info.h"

#include "mesh_library.h"
#include "model.h"
#include "shader_library.h"
#include "texture_library.h"


ModelRenderInfo::ModelRenderInfo(const Model& parentModel, MeshLibrary& meshLibrary, TextureLibrary& textureLibrary, ShaderLibrary& shaderLibrary)
	: mParentModel(parentModel)
	, mMeshInfo(meshLibrary.getMesh(parentModel.getMeshName()))
	, mTexture(textureLibrary.getTexture(parentModel.getTextureName()))
	, mShader(shaderLibrary.getShader(parentModel.getShaderName()))
{}


const Shader* ModelRenderInfo::getShader() const
{
	return mShader;
}


const Mesh* ModelRenderInfo::getMesh() const
{
	return mMeshInfo;
}


const Texture* ModelRenderInfo::getTexture() const
{
	return mTexture;
}
