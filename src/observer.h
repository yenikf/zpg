#pragma once

#include <glm/mat4x4.hpp>

#include <unordered_set>


class Subscriber
{
public:
	virtual void updateProjectionMatrix(glm::mat4 projectionMatrix) const = 0;
	virtual void updateViewMatrix(glm::mat4 viewMatrix) const = 0;
};


class Mediator
{
public:
	virtual ~Mediator()
	{};

	const Mediator& registerMediator() const;

	void registerSubscriber(const Subscriber* const s);
	void unregisterSubscriber(const Subscriber* const s);

	void notifyNewProjection(glm::mat4 projectionMatrix) const;
	void notifyNewView(glm::mat4 viewMatrix) const;

private:
	std::unordered_set<const Subscriber*> mSubscribers;
};


class Publisher
{
public:
	virtual ~Publisher()
	{};

private:
	const Mediator* mMediator;
};
