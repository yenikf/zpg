#include "texture.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image/stb_image.h>

#include <iostream>

#include "shader.h"


Texture::Texture(const std::string& filename)
	: mTextureFile(filename)
{
	stbi_set_flip_vertically_on_load(true);
	if (unsigned char* textureFile = stbi_load(filename.c_str(), &mWidth, &mHeight, &mNrChannels, STBI_rgb_alpha); textureFile) {
		glGenTextures(1, &mTextureID);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mTextureID);

		// texture resize
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// generate texture and mipmaps
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, mWidth, mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureFile);
		glGenerateMipmap(GL_TEXTURE_2D);

		std::cerr << "Texture file '" << filename << "' succesfully loaded.\n";
		stbi_image_free(textureFile);
	} else {
		std::cerr << "Error reading texture file " << filename << '\n';
	}
}


void Texture::drawTexture(const Shader& shader) const
{
	glBindTexture(GL_TEXTURE_2D, mTextureID);

	shader.sendTexture();
}
