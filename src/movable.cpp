#include "movable.h"

#include "position.h"


Movable::Movable(Position pos)
	: mPosition(std::move(pos))
{}


const Position& Movable::getPosition() const
{
	return mPosition;
}


glm::vec3 Movable::getPositionCoordinates() const
{
	return mPosition.getPositionCoordinates();
}


Movable& Movable::moveX(float distance)
{
	mPosition.moveX(distance);
	return *this;
}


Movable& Movable::moveY(float distance)
{
	mPosition.moveY(distance);
	return *this;
}


Movable& Movable::moveZ(float distance)
{
	mPosition.moveZ(distance);
	return *this;
}


Movable& Movable::moveForward(float distance)
{
	mPosition.moveForward(distance);
	return *this;
}


Movable& Movable::moveLeft(float distance)
{
	mPosition.moveLeft(distance);
	return *this;
}


Movable& Movable::moveUp(float distance)
{
	mPosition.moveUp(distance);
	return *this;
}


Movable& Movable::lookUp(float degrees)
{
	mPosition.lookUp(degrees);
	return *this;
}


Movable& Movable::lookLeft(float degrees)
{
	mPosition.lookLeft(degrees);
	return *this;
}


Movable& Movable::scale(float factor)
{
	mPosition.scale(factor);
	return *this;
}
