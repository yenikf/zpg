#include "texture_library.h"


const std::string TextureLibrary::defaultPath = "../textures/";


const Texture* TextureLibrary::getTexture(const std::string& name)
{
	auto texture = mTextures.find(name);
	if (texture != mTextures.end()) {
		return texture->second.get();
	} else {
		std::string filename = defaultPath + name;
		mTextures.try_emplace(name, std::make_unique<Texture>(filename));
		return getTexture(name);
	}
}
