#include "entity_render_info.h"

#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>

#include "entity.h"
#include "mesh.h"
#include "shader.h"
#include "texture.h"


EntityRenderInfo::EntityRenderInfo(const Entity& parentEntity, MeshLibrary& meshLibrary, TextureLibrary& textureLibrary, ShaderLibrary& shaderLibrary)
	: ModelRenderInfo(parentEntity, meshLibrary, textureLibrary, shaderLibrary)
	, mParentEntity(parentEntity)
{}


void EntityRenderInfo::drawEntity() const
{
	if (mParentEntity.isVisible()) {
		//	m_shader.use_this_shader();

		sendPosition();

		const Texture* texture = this->getTexture();
		if (texture) {
			texture->drawTexture(*this->getShader());
		}

		const Mesh* mesh = this->getMesh();
		if (mesh) {
			mesh->drawMesh();
		}
	}
}


void EntityRenderInfo::sendPosition() const
{
	glm::mat4 modelMatrix = mParentEntity.getPosition().getPositionMatrix();
	this->getShader()->sendModelMatrix(modelMatrix);
}
