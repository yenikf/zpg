#pragma once

#include <memory>
#include <vector>

#include "../scene.h"


class Entity;
class Lighting;


class Spheres : public Scene
{
public:
	Spheres();

	virtual void setCameras(Window& window) const override;
	virtual void setRenderedObjects(Renderer& renderer) const override;
	virtual void update() override;

private:
	std::vector<std::unique_ptr<Entity>> mEntities;
	std::unique_ptr<Lighting> mLight;
};
