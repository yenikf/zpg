#pragma once

#include <memory>
#include <vector>

#include "../scene.h"


class Entity;
class Skydome;


class TestScene : public Scene
{
public:
	TestScene();

	virtual void setCameras(Window& window) const override;
	virtual void setRenderedObjects(Renderer& renderer) const override;
	virtual void update() override;

private:
	std::unique_ptr<Entity> mEntity;
	std::unique_ptr<Entity> mCamera;
	std::unique_ptr<Skydome> mSkybox;
};