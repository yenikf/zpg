#include "test_scene.h"

#include "../entity.h"
#include "../position.h"
#include "../renderer.h"
#include "../skydome.h"
#include "../window.h"


TestScene::TestScene()
{
	mEntity = std::make_unique<Entity>(Position(glm::vec3(0.0f, -1.0f, 0.0f), 0.0f, 0.0f),
		"zombie", "zombie.png", "unlit", true);
	mCamera = std::make_unique<Entity>(Position(glm::vec3(0.0f, 0.0f, -2.0f), 0.0f, 0.0f),
		"", "", "unlit", false);
	mSkybox = std::make_unique<Skydome>();
}


void TestScene::setCameras(Window& window) const
{
	window.registerCameraObject(mCamera.get());
}


void TestScene::setRenderedObjects(Renderer& renderer) const
{
	renderer.registerForRendering(*mEntity.get());

	renderer.setSkybox(*mSkybox);
}


void TestScene::update()
{
	mEntity->lookLeft(0.4);
}

