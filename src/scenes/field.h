#pragma once

#include <memory>
#include <vector>

#include "../scene.h"


class Entity;
class Lighting;
class Model;
class Renderer;
class Skydome;


class Field : public Scene
{
public:
	Field();

	virtual void setCameras(Window& window) const override;
	virtual void setRenderedObjects(Renderer& renderer) const override;
	virtual void update() override;
	virtual void actionAt(int object, glm::vec3 worldCoordinates) override;

private:
	std::vector<std::unique_ptr<Entity>> mEntities;
	std::unique_ptr<Model> mSkyBox;
	std::unique_ptr<Lighting> mLight;

	mutable Renderer* mRenderer{};
};

