#include "field.h"

#include "../entity.h"
#include "../lighting.h"
#include "../model.h"
#include "../position.h"
#include "../renderer.h"
#include "../skydome.h"
#include "../window.h"


Field::Field()
{
	mEntities.push_back(
		std::make_unique<Entity>(Position(glm::vec3(0.0f, -1.0f, 0.0f), 0.0f, 0.0f),
			"teren", "grass.png", "blinn_phong", true));
	mEntities.push_back(
		std::make_unique<Entity>(Position(glm::vec3(-1.0f, 1.0f, -3.0f), 0.0f, 0.0f),
			"", "", "phong", true));
	mEntities.push_back(
		std::make_unique<Entity>(Position(glm::vec3(-20.0f, 2.0f, 20.0f), 135.0f, 0.0f),
			"", "", "phong", true));
	mEntities.push_back(
		std::make_unique<Entity>(Position(glm::vec3(-20.0f, -1.0f, -10.0f), 25.0f, 0.0f),
			"house", "house.png", "phong", true));
	mEntities.push_back(
		std::make_unique<Entity>(Position(glm::vec3(10.0f, -1.0f, -12.0f), 0.0f, 0.0f),
			"zombie", "zombie.png", "blinn_phong", true));

	mSkyBox = std::make_unique<Skydome>();

	mLight = std::make_unique<Lighting>(Position(glm::vec3(-12.0f, 2.0f, 6.0f), 0.0f, 0.0f));
}


void Field::setCameras(Window& window) const
{
	window.registerCameraObject(mEntities[1].get());
	window.registerCameraObject(mEntities[2].get());
}


void Field::setRenderedObjects(Renderer& renderer) const
{
	mRenderer = &renderer;

	renderer.registerForRendering(*mEntities[0]);
	renderer.registerForRendering(*mEntities[3]);
	renderer.registerForRendering(*mEntities[4]);

	renderer.setSkybox(*mSkyBox);

	renderer.registerLighting(*mLight);
}


void Field::update()
{
	mLight->lookLeft(0.2f);
	mLight->moveForward(0.05f);
}


void Field::actionAt(int object, glm::vec3 worldCoordinates)
{
	// the first rendered entity - teren.
	if (object == 0) {
		mEntities.push_back(
			std::make_unique<Entity>(Position(worldCoordinates, 0.0f, 0.0f),
				"tree", "tree.png", "blinn_phong", true));
		mEntities.back()->scale(0.2);
		mRenderer->registerForRendering(*mEntities.back());
	}
}

