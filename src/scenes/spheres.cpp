#include "spheres.h"

#include "../entity.h"
#include "../lighting.h"
#include "../position.h"
#include "../renderer.h"
#include "../window.h"


Spheres::Spheres()
{
	mEntities.push_back(
		std::make_unique<Entity>(Position(glm::vec3(0.0f, 0.0f, 0.0f), 0.0f, 0.0f),
			"suzanne", "", "untextured_phong", true));
	mEntities.push_back(
		std::make_unique<Entity>(Position(glm::vec3(0.0f, 0.0f, -3.0f), 0.0f, 0.0f),
			"ico", "", "untextured_phong", true));
	mEntities.push_back(
		std::make_unique<Entity>(Position(glm::vec3(-3.0f, 0.0f, 0.0f), 0.0f, 0.0f),
			"ico", "", "untextured_phong", true));
	mEntities.push_back(
		std::make_unique<Entity>(Position(glm::vec3(0.0f, 0.0f, 3.0f), 0.0f, 0.0f),
			"ico", "", "untextured_blinn_phong", true));
	mEntities.push_back(
		std::make_unique<Entity>(Position(glm::vec3(3.0f, 0.0f, 0.0f), 0.0f, 0.0f),
			"ico", "", "untextured_blinn_phong", true));
	mEntities.push_back(
		std::make_unique<Entity>(Position(glm::vec3(0.0f, 0.0f, 16.0f), 0.0f, 0.0f),
			"suzanne", "", "untextured_phong", true));
	mEntities.push_back(
		std::make_unique<Entity>(Position(glm::vec3(0.0f, -2.0f, 0.0f), 0.0f, 0.0f),
			"zombie", "", "untextured_phong", true));
	mEntities.push_back(
		std::make_unique<Entity>(Position(glm::vec3(-2.0f, 3.0f, -4.0f), 0.0f, 0.0f),
			"zombie", "", "untextured_blinn_phong", true));
	mLight = std::make_unique<Lighting>(Position(glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, 0.0f));
}


void Spheres::setCameras(Window& window) const
{
	window.registerCameraObject(mEntities[0].get());
	window.registerCameraObject(mEntities[2].get());
}


void Spheres::setRenderedObjects(Renderer& renderer) const
{
	for (const auto& entity : mEntities) {
		renderer.registerForRendering(*entity);
	}
	renderer.registerLighting(*mLight);
}


void Spheres::update()
{
	mEntities[5]->lookLeft(0.4f);
}

