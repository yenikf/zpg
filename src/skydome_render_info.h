#pragma once

#include "model_render_info.h"


class MeshLibrary;
class ShaderLibrary;
class Model;
class TextureLibrary;


class SkydomeRenderInfo : public ModelRenderInfo
{
public:
	SkydomeRenderInfo(const Model& parentSkydome, MeshLibrary& meshLibrary, TextureLibrary& textureLibrary, ShaderLibrary& shaderLibrary);

	virtual void drawEntity() const override;

private:
};