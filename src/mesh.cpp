#include "mesh.h"


Mesh::Mesh(const std::vector<float>& vertices)
	: mVertices(vertices)
{
	createVertexBufferHandles(mVAO, mVertices);
}


void Mesh::drawMesh() const
{
	glBindVertexArray(mVAO[0]);
	glDrawArrays(GL_TRIANGLES, 0, mVertices.size() / valuesPerVertex); // mode, first, count
}


void Mesh::createVertexBufferHandles(std::array<GLuint, 1>& vao, const std::vector<float>& vertices)
{
	glGenVertexArrays(1, vao.data());
	glBindVertexArray(vao[0]); // enable vao

	std::array<GLuint, 1> vbo{};
	glGenBuffers(1, vbo.data()); // generate the buffer object
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); // bind the buffer as vertex buffer
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), vertices.data(), GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); // enable vertex attributes
	glEnableVertexAttribArray(1); // normal vector
	glEnableVertexAttribArray(2); // texture coordinates
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * valuesPerVertex, 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float) * valuesPerVertex, (GLvoid*)(sizeof(float) * 3));
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(float) * valuesPerVertex, (GLvoid*)(sizeof(float) * 6));
}
