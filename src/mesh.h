#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <array>
#include <vector>


constexpr int valuesPerVertex = 8;


class Mesh
{
public:
	explicit Mesh(const std::vector<float>& vertices);

	void drawMesh() const;

private:
	void createVertexBufferHandles(std::array<GLuint, 1>& vao, const std::vector<float>& vertices);

	std::array<GLuint, 1> mVAO{}; // vertex array object (VAO)
	std::vector<float> mVertices;
};

