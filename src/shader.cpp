#include "shader.h"

#include <glm/vec3.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>

#include "utility.h"


Shader::Shader(const std::string& vertexShaderFilename, const std::string& fragmentShaderFilename)
{
	std::cerr << "Building shader " << vertexShaderFilename << '\n';

	std::string vertexShaderSource = readTextFile(vertexShaderFilename);
	std::string fragmentShaderSource = readTextFile(fragmentShaderFilename);

	GLuint vertexShader = compileShader(GL_VERTEX_SHADER, vertexShaderSource);
	GLuint fragmentShader = compileShader(GL_FRAGMENT_SHADER, fragmentShaderSource);

	mShaderHandle = linkShaders(vertexShader, fragmentShader);

	// set matrices
	mModelHandle = getShaderVariable(mShaderHandle, modelMatrixName);
	mViewHandle = getShaderVariable(mShaderHandle, viewMatrixName);
	mProjectionHandle = getShaderVariable(mShaderHandle, projectionMatrixName);
	mTextureHandle = getShaderVariable(mShaderHandle, textureName);
	mLightingHandle = getShaderVariable(mShaderHandle, lightName);
}


// NOTE useThisShader might be possibly private.
void Shader::useThisShader() const
{
	glUseProgram(mShaderHandle);
}


void Shader::sendModelMatrix(const glm::mat4& matrix) const
{
	useThisShader();
	sendUniform(mModelHandle, matrix);
}


void Shader::sendViewMatrix(const glm::mat4& viewMatrix) const
{
	useThisShader();
	sendUniform(mViewHandle, viewMatrix);
}


void Shader::sendProjectionMatrix(const glm::mat4& projectionMatrix) const
{
	useThisShader();
	sendUniform(mProjectionHandle, projectionMatrix);
}


void Shader::sendTexture() const
{
	useThisShader();
	// TODO Textures render the same without this, why?
//	sendUniform(mTextureHandle, 0);
}


void Shader::sendLighting(const glm::vec3& position) const
{
	useThisShader();
	sendUniform(mLightingHandle, position);
}


void Shader::updateViewMatrix(glm::mat4 viewMatrix) const
{
	sendViewMatrix(viewMatrix);
}


void Shader::updateProjectionMatrix(glm::mat4 projectionMatrix) const
{
	sendProjectionMatrix(projectionMatrix);
}


GLuint Shader::compileShader(GLenum shaderType, const std::string& source)
{
	char const* sourceText = source.c_str();

	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &sourceText, 0);
	glCompileShader(shader);

	// check for errors
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE) {
		GLint infoLogLength;
		glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
		std::string infoLog(infoLogLength, '\0');
		glGetShaderInfoLog(shader, infoLogLength, nullptr, infoLog.data());
		std::cerr << "Shader compile failure: " << infoLog.data() << '\n';
	}
	return shader;
}


GLuint Shader::linkShaders(GLuint vertexShader, GLuint fragmentShader)
{
	GLuint shaderHandle = glCreateProgram();
	glAttachShader(shaderHandle, vertexShader);
	glAttachShader(shaderHandle, fragmentShader);
	glLinkProgram(shaderHandle);

	// check for errors
	GLint status;
	glGetProgramiv(shaderHandle, GL_LINK_STATUS, &status);
	if (status == GL_FALSE) {
		GLint infoLogLength;
		glGetProgramiv(shaderHandle, GL_INFO_LOG_LENGTH, &infoLogLength);
		std::string infoLog(infoLogLength, '\0');
		glGetShaderInfoLog(shaderHandle, infoLogLength, nullptr, infoLog.data());
		std::cerr << "Shader linking failure: " << infoLog.data() << '\n';
	}

	return shaderHandle;
}


GLint Shader::getShaderVariable(GLuint shaderProgram, const std::string& varName)
{
	GLint varHandle = glGetUniformLocation(shaderProgram, varName.c_str());
	if (varHandle == -1) {
		std::cerr << "No shader variable '" << varName << "' found\n";
	}
	return varHandle;
}


void Shader::sendUniform(GLint uniformHandle, int i) const
{
	glUniform1i(uniformHandle, 0);
}


void Shader::sendUniform(GLint uniformHandle, const glm::vec3& vec) const
{
	glUniform3f(uniformHandle, vec.x, vec.y, vec.z);
}


void Shader::sendUniform(GLint uniformHandle, const glm::mat4& matrix) const
{
	glUniformMatrix4fv(uniformHandle, 1, GL_FALSE, &matrix[0][0]);
}

