#pragma once


class Mesh;
class MeshLibrary;
class Model;
class Shader;
class ShaderLibrary;
class Texture;
class TextureLibrary;


class ModelRenderInfo
{
public:
	ModelRenderInfo(const Model& parentModel, MeshLibrary& meshLibrary, TextureLibrary& textureLibrary, ShaderLibrary& shaderLibrary);

	virtual void drawEntity() const = 0;

	const Shader* getShader() const;

protected:
	const Mesh* getMesh() const;
	const Texture* getTexture() const;

private:
	const Mesh* mMeshInfo;
	const Shader* mShader;
	const Texture* mTexture;

	const Model& mParentModel;
};
