#pragma once


class Scene;
class Window;


class Controller
{
public:
	static Controller& instance();

	Controller(const Controller& other) = delete;
	Controller& operator=(const Controller& other) = delete;
	Controller(Controller&& other) = delete;
	Controller& operator=(Controller&& other) = delete;

	void attachWindow(Window* window);
	void attachScene(Scene* scene);
	void handleInput();

	static void onCursorPosition(double x, double y);
	static void onMouseButton(int button, int action, int mode);
	static void onKeyPress(int key, int scancode, int action, int mods);
	static void onWindowFocus(int focused);
	static void onWindowIconify(int iconified);
	static void onWindowSizeChange(int width, int height);

private:
	Controller() = default;
	~Controller() = default;

	void setCallbacks(Window* window);
	void releaseCallbacks(Window* window);

	static bool wPressed;
	static bool sPressed;
	static bool aPressed;
	static bool dPressed;
	static bool spacePressed;
	static bool shiftPressed;
	static bool mouseLook;
	static bool mouseModeSwitched;

	static constexpr float movementSpeed{ 0.1f };
	static constexpr float mouseLookDivisor{ 5.0f };

	Window* mControlledWindow{};
	Scene* mControlledScene{};
};
