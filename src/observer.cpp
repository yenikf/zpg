#include "observer.h"


const Mediator& Mediator::registerMediator() const
{
	return *this;
}


void Mediator::registerSubscriber(const Subscriber* const s)
{
	if (s) {
		mSubscribers.insert(s);
	}
}


void Mediator::unregisterSubscriber(const Subscriber* const s)
{
	if (s) {
		mSubscribers.erase(s);
	}
}


void Mediator::notifyNewProjection(glm::mat4 projectionMatrix) const
{
	for (auto s : mSubscribers) {
		s->updateProjectionMatrix(projectionMatrix);
	}
}


void Mediator::notifyNewView(glm::mat4 viewMatrix) const
{
	for (auto s : mSubscribers) {
		s->updateViewMatrix(viewMatrix);
	}
}
