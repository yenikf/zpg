#include "shader_library.h"

#include <iostream>


const std::string ShaderLibrary::defaultPath = "../shaders/";
const std::string ShaderLibrary::defaultVertexShaderSuffix = "_vertex_shader.vert";
const std::string ShaderLibrary::defaultFragmentShaderSuffix = "_fragment_shader.frag";


const Shader* ShaderLibrary::getShader(const std::string& name)
{
	auto shader = mShaders.find(name);
	if (shader != mShaders.end()) {
		return shader->second.get();
	} else {
		std::string vertexShaderName = defaultPath + name + defaultVertexShaderSuffix;
		std::string fragmentShaderName = defaultPath + name + defaultFragmentShaderSuffix;
		mShaders.try_emplace(name, std::make_unique<Shader>(vertexShaderName, fragmentShaderName));
		return getShader(name);
	}
}

