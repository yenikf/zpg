#include "renderer.h"

#include <iostream>

#include "camera.h"
#include "entity.h"
#include "entity_render_info.h"
#include "lighting.h"
#include "model.h"
#include "model_render_info.h"
#include "skydome_render_info.h"
#include "window.h"


Renderer::Renderer()
{
	if (!glfwInit()) {
		std::cerr << "ERROR: could not start GLFW3\n";
		exit(EXIT_FAILURE);
	}

	glfwSetErrorCallback([](int error, const char* description) { std::cerr << description << '\n'; });
}


Renderer::~Renderer()
{
	//	glfwTerminate();
}


void Renderer::printVersionInfo() const
{
	std::cerr << "OpenGL Version: " << glGetString(GL_VERSION) << '\n';
	std::cerr << "Using GLEW " << glewGetString(GLEW_VERSION) << '\n';
	std::cerr << "Vendor " << glGetString(GL_VENDOR) << '\n';
	std::cerr << "Renderer " << glGetString(GL_RENDERER) << '\n';
	std::cerr << "GLSL " << glGetString(GL_SHADING_LANGUAGE_VERSION) << '\n';
	int major, minor, revision;
	glfwGetVersion(&major, &minor, &revision);
	std::cerr << "Using GLFW " << major << '.' << minor << '.' << revision << '\n';
}


void Renderer::registerForRendering(const Entity& renderedEntity)
{
	auto newRenderInfo = std::make_unique<EntityRenderInfo>(renderedEntity, mMeshLibrary, mTextureLibrary, mShaderLibrary);
	mRenderedObjects.push_back(std::move(newRenderInfo));
	registerForNotifications(*mRenderedObjects.back());

	std::cerr << "The entity is registered for rendering\n";
}


void Renderer::unregister(const Entity& renderedEntity)
{
	// TODO Implement Renderer::unregister member function.
}


void Renderer::registerLighting(const Lighting& lighting)
{
	mLighting = &lighting;
}


void Renderer::setSkybox(const Model& skybox)
{
	mSkybox = std::make_unique<SkydomeRenderInfo>(skybox, mMeshLibrary, mTextureLibrary, mShaderLibrary);
	registerForNotifications(*mSkybox);

	std::cerr << "The skybox is created\n";
}


Window* Renderer::createWindow(int width, int height, const char* title)
{
	mWindow = std::make_unique<Window>(*this, width, height, title);
	initGlew();

	return mWindow.get();
}


void Renderer::renderScene()
{
	if (mWindow) {
		renderFrame();
	} else {
		std::cerr << "No window registered for rendering\n";
	}
}


std::pair<int, float> Renderer::identifyObject(double x, double y) const
{
	GLfloat depth{};
	glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth);
	GLuint index{};
	glReadPixels(x, y, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_INT, &index);
	return { index, depth };
}


void Renderer::registerForNotifications(const ModelRenderInfo& renderInfo)
{
	//Camera& activeCamera = mWindow->getActiveCamera();
	// TODO Make it cleaner. ObserverManager should help. Maybe take the shaders directly from the ShaderLibrary.
	//activeCamera.registerSubscriber(&(renderInfo.getShader()));

	registerSubscriber(renderInfo.getShader());
	if (mWindow) {
		// TODO Reintroducing the hack: need this for the newly added objects
		mWindow->startCamera();
	}
}


void Renderer::initGlew()
{
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		std::cerr << "ERROR: could not initialize GLEW\n";
		exit(EXIT_FAILURE);
	}
}


void Renderer::renderFrame()
{
	// clear color and depth buffer
	mWindow->clearScreen();

	if (mSkybox) {
		mSkybox->drawEntity();
	}

	// draw_models
	for (size_t i = 0; i < mRenderedObjects.size(); ++i) {
		glStencilFunc(GL_ALWAYS, i, 0xFF);
		mRenderedObjects[i]->drawEntity();

		if (mLighting) {
			mRenderedObjects[i]->getShader()->sendLighting(mLighting->getPositionCoordinates());
		}
	}
	//for (const auto& object : mRenderedObjects) {
	//	object->drawEntity();
	//}

	// update other events like input handling
	glfwPollEvents();

	// put the stuff we’ve been drawing onto the display
	mWindow->finishFrame();
}

