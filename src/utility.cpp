#include "utility.h"

#include <fstream>
#include <iostream>


std::string readTextFile(std::string const& filename)
{
	std::ifstream ifs(filename);
	if (!ifs.is_open()) {
		std::cerr << "Error opening file '" << filename << "'\n";
		std::exit(EXIT_FAILURE);
	}
	std::string s;
	std::getline(ifs, s, '\0');
	return s;
}
