#include "position.h"

#include <glm/gtc/matrix_transform.hpp>

#include <algorithm>


Position::Position(glm::vec3 pos, float horizAngle, float vertAngle)
	: mPositionPoint(pos), mHorizontalAngle(horizAngle), mVerticalAngle(vertAngle)
{}


glm::vec3 Position::getPositionCoordinates() const
{
	return mPositionPoint;
}


const glm::mat4 Position::getPositionMatrix() const
{
	glm::mat4 matrix = glm::mat4(1.0f);
	matrix = glm::scale(matrix, glm::vec3(mScaleFactor, mScaleFactor, mScaleFactor));
	matrix = glm::rotate(matrix, glm::radians(mVerticalAngle), glm::vec3(0.0f, 0.0f, 1.0f));
	matrix = glm::rotate(matrix, glm::radians(mHorizontalAngle), glm::vec3(0.0f, 1.0f, 0.0f));
	matrix[3][0] += mPositionPoint.x;
	matrix[3][1] += mPositionPoint.y;
	matrix[3][2] += mPositionPoint.z;
	return matrix;
}


const glm::mat4 Position::getViewMatrix() const
{
	return glm::lookAt(mPositionPoint, mPositionPoint + getViewVector(), mUpVector);
}


Position& Position::moveX(float distance)
{
	mPositionPoint.x += distance;
	return *this;
}


Position& Position::moveY(float distance)
{
	mPositionPoint.y += distance;
	return *this;
}


Position& Position::moveZ(float distance)
{
	mPositionPoint.z += distance;
	return *this;
}


Position& Position::moveForward(float distance)
{
	mPositionPoint += glm::normalize(getViewVector()) * distance;
	return *this;
}


Position& Position::moveLeft(float distance)
{
	glm::vec3 left_vector = glm::normalize(glm::cross(mUpVector, getViewVector()));
	mPositionPoint += left_vector * distance;
	return *this;
}


Position& Position::moveUp(float distance)
{
	mPositionPoint += glm::normalize(mUpVector) * distance;
	return *this;
}


Position& Position::lookUp(float degrees)
{
	mVerticalAngle = std::clamp(mVerticalAngle - degrees, -89.9f, 89.9f);
	return *this;
}


Position& Position::lookLeft(float degrees)
{
	mHorizontalAngle += degrees;
	return *this;
}


Position& Position::scale(float factor)
{
	mScaleFactor = factor;
	return *this;
}


glm::vec3 Position::getViewVector() const
{
	float hor_rad = glm::radians(mHorizontalAngle);
	float vert_rad = glm::radians(mVerticalAngle);
	return { glm::cos(vert_rad) * glm::sin(hor_rad), glm::sin(vert_rad) , glm::cos(vert_rad) * glm::cos(hor_rad) };
}
