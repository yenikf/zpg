#include "model.h"


Model::Model(std::string meshName, std::string textureName, std::string shaderName)
	: mMesh(std::move(meshName))
	, mTexture(std::move(textureName))
	, mShader(std::move(shaderName))
{}


const std::string& Model::getMeshName() const
{
	return mMesh;
}


const std::string& Model::getTextureName() const
{
	return mTexture;
}


const std::string& Model::getShaderName() const
{
	return mShader;
}
