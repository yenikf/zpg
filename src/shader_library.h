#pragma once

#include <memory>
#include <string>
#include <unordered_map>

#include "shader.h"


class ShaderLibrary
{
public:
	const Shader* getShader(const std::string& name);

private:
	static const std::string defaultPath;
	static const std::string defaultVertexShaderSuffix;
	static const std::string defaultFragmentShaderSuffix;

	std::unordered_map<std::string, std::unique_ptr<const Shader>> mShaders;
};

