#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>


class Shader;


class Texture
{
public:
	Texture(const std::string& filename);

	void drawTexture(const Shader& shader) const;

private:
	GLuint mTextureID{};

	std::string mTextureFile;
	int mWidth{};
	int mHeight{};
	int mNrChannels{};
};
