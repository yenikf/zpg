#pragma once

#include <memory>
#include <string>
#include <unordered_map>

#include "texture.h"


class TextureLibrary
{
public:
	const Texture* getTexture(const std::string& name);

private:
	static const std::string defaultPath;

	std::unordered_map<std::string, std::unique_ptr<const Texture>> mTextures;
};

