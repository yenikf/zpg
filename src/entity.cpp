#include "entity.h"


// TODO Add Material and MaterialLibrary classes.

Entity::Entity(Position pos, std::string meshName, std::string textureName, std::string shaderName, bool visible)
	: Model(meshName, textureName, shaderName)
	, Movable(pos)
	, mVisible(visible)
{}


void Entity::setVisible(bool visible)
{
	mVisible = visible;
}


bool Entity::isVisible() const
{
	return mVisible;
}


Entity& Entity::possess()
{
	mPossessed = true;
	return *this;
}


Entity& Entity::release()
{
	mPossessed = false;
	return *this;
}


bool Entity::isPossessed() const
{
	return mPossessed;
}
