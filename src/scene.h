#pragma once

#include <glm/vec3.hpp>


class Renderer;
class Window;


class Scene
{
public:
	virtual ~Scene() = default;

	virtual void setCameras(Window& window) const = 0;
	virtual void setRenderedObjects(Renderer& renderer) const = 0;

	virtual void update()
	{};
	virtual void actionAt(int object, glm::vec3 worldCoordinates)
	{};
};
