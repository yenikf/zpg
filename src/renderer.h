#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <memory>
#include <utility>
#include <vector>

#include "mesh_library.h"
#include "observer.h"
#include "shader_library.h"
#include "texture_library.h"


class Camera;
class Entity;
class EntityRenderInfo;
class Lighting;
class Model;
class ModelRenderInfo;
class SkydomeRenderInfo;
class Window;


class Renderer : public Mediator
{
public:
	Renderer();
	~Renderer();

	void printVersionInfo() const;
	void registerForRendering(const Entity& renderedEntity);
	void unregister(const Entity& renderedEntity);
	void registerLighting(const Lighting& lighting);
	void setSkybox(const Model& skybox);

	Window* createWindow(int width, int height, const char* title);
	void renderScene();

	std::pair<int, float> identifyObject(double x, double y) const;

private:
	void registerForNotifications(const ModelRenderInfo& renderInfo);
	void initGlew();
	void renderFrame();

	std::vector<std::unique_ptr<EntityRenderInfo>> mRenderedObjects;
	std::unique_ptr<SkydomeRenderInfo> mSkybox;
	std::unique_ptr<Window> mWindow;
	const Lighting* mLighting{};

	MeshLibrary mMeshLibrary;
	ShaderLibrary mShaderLibrary;
	TextureLibrary mTextureLibrary;
};
