#include "mesh_library.h"

#include "assimp/Importer.hpp"
#include "assimp/postprocess.h"
#include "assimp/scene.h"

#include <iostream>


const std::string MeshLibrary::defaultPath = "../models/";
const std::string MeshLibrary::defaultType = ".obj";


const Mesh* MeshLibrary::getMesh(const std::string& name)
{
	auto model = mMeshInfos.find(name);
	if (model != mMeshInfos.end()) {
		return model->second.get();
	} else {
		std::vector<float> modelVertices = loadMesh(name);
		registerMesh(name, modelVertices);
		return getMesh(name);
	}
}


std::vector<float> MeshLibrary::loadMesh(const std::string& name)
{
	Assimp::Importer importer;
	unsigned int importOptions = aiProcess_Triangulate
		| aiProcess_OptimizeMeshes              // sloučení malých plošek
		| aiProcess_JoinIdenticalVertices       // NUTNÉ jinak hodně duplikuje
		| aiProcess_Triangulate                 // prevod vsech ploch na trojuhelniky
		| aiProcess_CalcTangentSpace;           // vypocet tangenty, nutny pro spravne pouziti normalove mapy

	std::string filename = defaultPath + name + defaultType;
	const aiScene* model = importer.ReadFile(filename, importOptions);
	std::vector<float> vertices;
	if (model) {
		std::cerr << "Model " << filename << " loaded succesfully\n";
		aiMesh* mesh = model->mMeshes[0];
		for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
			for (unsigned int j = 0; j < 3; j++) {
				vertices.push_back(mesh->mVertices[mesh->mFaces[i].mIndices[j]].x);
				vertices.push_back(mesh->mVertices[mesh->mFaces[i].mIndices[j]].y);
				vertices.push_back(mesh->mVertices[mesh->mFaces[i].mIndices[j]].z);
				vertices.push_back(mesh->mNormals[mesh->mFaces[i].mIndices[j]].x);
				vertices.push_back(mesh->mNormals[mesh->mFaces[i].mIndices[j]].y);
				vertices.push_back(mesh->mNormals[mesh->mFaces[i].mIndices[j]].z);
				vertices.push_back(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[j]].x);
				vertices.push_back(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[j]].y);
			}
		}
	} else {
		std::cerr << "Error loading model " << filename << '\n';
	}
	return vertices;
}


void MeshLibrary::registerMesh(const std::string& name, const std::vector<float>& vertices)
{
	mMeshInfos.try_emplace(name, std::make_unique<Mesh>(vertices));
}
