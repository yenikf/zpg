#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/mat4x4.hpp>

#include <string>

#include "observer.h"


static const char* modelMatrixName = "model_position_matrix";
static const char* viewMatrixName = "camera_view_matrix";
static const char* projectionMatrixName = "projection_matrix";
static const char* textureName = "texture_image";
static const char* lightName = "light_position";


class Shader : public Subscriber
{
public:
	Shader(const std::string& vertexShaderFilename, const std::string& fragmentShaderFilename);

	void useThisShader() const;
	void sendModelMatrix(const glm::mat4& matrix) const;
	void sendViewMatrix(const glm::mat4& viewMatrix) const;
	void sendProjectionMatrix(const glm::mat4& projectionMatrix) const;
	void sendTexture() const;
	void sendLighting(const glm::vec3& position) const;

	virtual void updateViewMatrix(glm::mat4 viewMatrix) const override;
	virtual void updateProjectionMatrix(glm::mat4 projectionMatrix) const override;

private:
	GLuint compileShader(GLenum shaderType, const std::string& source);
	GLuint linkShaders(GLuint vertexShader, GLuint fragmentShader);
	GLint getShaderVariable(GLuint shaderProgram, const std::string& varName);
	void sendUniform(GLint uniformHandle, int i) const;
	void sendUniform(GLint uniformHandle, const glm::vec3& vec) const;
	void sendUniform(GLint uniformHandle, const glm::mat4& matrix) const;

	GLuint mShaderHandle{};
	GLint mModelHandle{};
	GLint mViewHandle{};
	GLint mProjectionHandle{};
	GLint mTextureHandle{};
	GLint mLightingHandle{};
};

