#include "skydome_render_info.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "mesh.h"
#include "texture.h"


SkydomeRenderInfo::SkydomeRenderInfo(const Model& parentModel, MeshLibrary& meshLibrary, TextureLibrary& textureLibrary, ShaderLibrary& shaderLibrary)
	: ModelRenderInfo(parentModel, meshLibrary, textureLibrary, shaderLibrary)
{}


void SkydomeRenderInfo::drawEntity() const
{
	glDisable(GL_DEPTH_TEST);

	const Texture* texture = this->getTexture();
	if (texture) {
		texture->drawTexture(*this->getShader());
	}

	const Mesh* mesh = this->getMesh();
	if (mesh) {
		mesh->drawMesh();
	}

	glEnable(GL_DEPTH_TEST);
}
