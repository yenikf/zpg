#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <utility>
#include <vector>

#include "camera.h"


class Entity;
class Renderer;


class Window
{
public:
	Window(const Renderer& renderer, int windowWidth, int windowHeight, char const* windowTitle);
	~Window();

	void changeWindowSize(int width, int height);

	void close();
	bool shouldClose() const;

	void clearScreen();
	void finishFrame();

	void registerCameraObject(Entity* newCamera);
	void switchCamera();
	void startCamera();
	Camera& getActiveCamera();

	std::pair<int, glm::vec3> identifyObject(bool isAtCenter = false);
	void setMouselookView(bool mouseLookActive);

	void setCursorPositionCallback(void (cursorCallback)(GLFWwindow*, double, double));
	void setMouseButtonCallback(void (buttonCallback)(GLFWwindow* w, int button, int action, int mode));
	void setKeypushCallback(void (keyCallback)(GLFWwindow* window, int key, int scancode, int action, int mods));
	void setWindowFocusCallback(void (windowFocusCallback)(GLFWwindow* w, int focused));
	void setWindowIconifyCallback(void (windowIconifyCallback)(GLFWwindow* w, int iconified));
	void setWindowSizeCallback(void (windowSizeCallback)(GLFWwindow* w, int width, int height));

private:
	GLFWwindow* mWindowHandle{};
	int mWidth{};
	int mHeight{};

	const Renderer& mRenderer;
	Camera mCamera;
	std::vector<Entity*> mCameraEntities;
	decltype(mCameraEntities)::size_type mActiveCameraIndex{};
};
