
#include <memory>

#include "controller.h"
#include "renderer.h"
#include "scenes/field.h"
#include "scenes/spheres.h"
#include "scenes/test_scene.h"
#include "window.h"


int main(void)
{
	Renderer simpleRenderer;
	Window* mainWindow = simpleRenderer.createWindow(1024, 768, "ZPG");
	simpleRenderer.printVersionInfo();

	std::unique_ptr<Scene> scene = std::make_unique<Field>();
	scene->setRenderedObjects(simpleRenderer);
	scene->setCameras(*mainWindow);

	Controller::instance().attachWindow(mainWindow);
	Controller::instance().attachScene(scene.get());

	mainWindow->startCamera();
	while (!mainWindow->shouldClose()) {
		simpleRenderer.renderScene();

		scene->update();

		Controller::instance().handleInput();
	}

	return 0;
}


