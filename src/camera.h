#pragma once

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

#include "observer.h"


class Entity;
class Renderer;


class Camera : public Publisher
{
public:
	Camera(const Mediator& renderer, float width, float height);

	void setPosition(Entity* newPosition);
	void setProjection(float width, float height);

	void moveForward(float distance);
	void moveLeft(float distance);
	void moveUp(float distance);
	void lookUp(float degrees);
	void lookLeft(float degrees);

	glm::vec3 identifyCoordinates(int screenWidth, int screenHeight, double x, double y, float zDepth);

private:
	void updateView();
	void updateProjection();

	glm::mat4 getViewMatrix() const;

	Entity* mCameraPosition{};
	glm::mat4 mProjectionMatrix{};
	const Mediator& mRenderer;
};

