#pragma once

#include <string>


class Model
{
public:
	Model(std::string meshName, std::string textureName, std::string shaderName);

	const std::string& getMeshName() const;
	const std::string& getTextureName() const;
	const std::string& getShaderName() const;

private:
	std::string mMesh;
	std::string mTexture;
	std::string mShader;
};
