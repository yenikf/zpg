#pragma once

#include <string>

#include "model.h"
#include "movable.h"
#include "position.h"


class Entity : public Model, public Movable
{
public:
	Entity(Position pos, std::string meshName, std::string textureName, std::string shaderName, bool visible);

	void setVisible(bool visible);
	bool isVisible() const;

	Entity& possess();
	Entity& release();
	bool isPossessed() const;

private:
	bool mVisible{};
	bool mPossessed{};
};
