#include "camera.h"

#include <glm/gtc/matrix_transform.hpp>

#include "entity.h"
#include "renderer.h"


Camera::Camera(const Mediator& renderer, float width, float height)
	: mRenderer(renderer.registerMediator())
{
	setProjection(width, height);
}


void Camera::setPosition(Entity* newPosition)
{
	if (mCameraPosition) {
		mCameraPosition->setVisible(true);
		mCameraPosition->release();
	}

	mCameraPosition = newPosition;

	if (mCameraPosition) {
		mCameraPosition->possess();
		mCameraPosition->setVisible(false);

		updateView();
	}
}


void Camera::setProjection(float width, float height)
{
	mProjectionMatrix = glm::perspective(1.0472f, width / height, 0.1f, 1000.0f);
	updateProjection();
}


void Camera::moveForward(float distance)
{
	if (mCameraPosition) {
		mCameraPosition->moveForward(distance);
		updateView();
	}
}


void Camera::moveLeft(float distance)
{
	if (mCameraPosition) {
		mCameraPosition->moveLeft(distance);
		updateView();
	}
}


void Camera::moveUp(float distance)
{
	if (mCameraPosition) {
		mCameraPosition->moveUp(distance);
		updateView();
	}
}


void Camera::lookUp(float degrees)
{
	if (mCameraPosition) {
		mCameraPosition->lookUp(degrees);
		updateView();
	}
}


void Camera::lookLeft(float degrees)
{
	if (mCameraPosition) {
		mCameraPosition->lookLeft(degrees);
		updateView();
	}
}


glm::vec3 Camera::identifyCoordinates(int screenWidth, int screenHeight, double x, double y, float zDepth)
{
	glm::vec3 screenCoordinates = glm::vec3(x, y, zDepth);
	glm::mat4 viewMatrix = getViewMatrix();
	glm::vec4 viewPort = glm::vec4(0, 0, screenWidth, screenHeight);
	return glm::unProject(screenCoordinates, viewMatrix, mProjectionMatrix, viewPort);
}


void Camera::updateView()
{
	glm::mat4 newViewMatrix = getViewMatrix();
	mRenderer.notifyNewView(newViewMatrix);
}


void Camera::updateProjection()
{
	mRenderer.notifyNewProjection(mProjectionMatrix);
}


glm::mat4 Camera::getViewMatrix() const
{
	return mCameraPosition->getPosition().getViewMatrix();
}

