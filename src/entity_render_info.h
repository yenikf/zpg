#pragma once

#include "model_render_info.h"


class Entity;
class MeshLibrary;
class ShaderLibrary;
class TextureLibrary;


class EntityRenderInfo : public ModelRenderInfo
{
public:
	EntityRenderInfo(const Entity& parentEntity, MeshLibrary& meshLibrary, TextureLibrary& textureLibrary, ShaderLibrary& shaderLibrary);

	virtual void drawEntity() const override;

private:
	void sendPosition() const;

	const Entity& mParentEntity;
};

