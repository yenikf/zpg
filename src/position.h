#pragma once

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>


class Position
{
public:
	Position(glm::vec3 pos, float horizAngle, float vertAngle);

	glm::vec3 getPositionCoordinates() const;

	const glm::mat4 getPositionMatrix() const;
	const glm::mat4 getViewMatrix() const;

	Position& moveX(float distance);
	Position& moveY(float distance);
	Position& moveZ(float distance);
	Position& moveForward(float distance);
	Position& moveLeft(float distance);
	Position& moveUp(float distance);
	Position& lookUp(float degrees);
	Position& lookLeft(float degrees);
	Position& scale(float factor);

private:
	glm::vec3 getViewVector() const;

	glm::vec3 mPositionPoint{};
	glm::vec3 mUpVector{ 0.0f, 1.0f, 0.0f };
	float mHorizontalAngle{};
	float mVerticalAngle{};
	float mScaleFactor{ 1.0f };
};

